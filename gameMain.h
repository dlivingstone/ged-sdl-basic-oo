#ifndef GAME_MAIN
#define GAME_MAIN

#include <iostream>
#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_opengl.h>

// C stdlib and C time libraries for rand and time functions
#include <cstdlib>
#include <ctime>
// iostream for cin and cout
#include <iostream>
// stringstream and string
#include <sstream>
#include <string>

#include "label.h"
#include "rect.h"

class Game {
public:
	// Constructor and destructor methods
	Game(void);
	~Game();
	// init and run are the only functions that need called from main
	void init(void);
	void run(void);
	// displayString is a static (class) function that could potentially be called from
	// other classes, so is left as public
	static void displayString(float x, float y, const char * str, TTF_Font * textFont);
private:
	// setupRC will be called when game is created
	void setupRC(void);
	// The game loop will call update, draw and handle event methods
	//void update(void);
	void draw(void);
	void handleSDLEvent(SDL_Event const &sdlEvent);
	SDL_Window *window;
	SDL_GLContext context;

	int score;

	static int instances;

	clock_t lastTime; // clock_t is an integer type
	clock_t currentTime; // use this to track time between frames

	TTF_Font * textFont;	// SDL type for True-Type font rendering
	Label * playerLabel;
	Label * targetLabel;
	
	Rect * player;
	Rect * target;
};

#endif