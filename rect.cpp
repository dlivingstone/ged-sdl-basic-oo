#include "rect.h"
#include <SDL_opengl.h>

Rect::Rect(void) : xpos(0.0f), ypos(0.0f), width(0.1f), height(0.1f)
{
}

Rect::Rect(float x, float y, float w, float h) : xpos(x), ypos(y), width(w), height(h) 
{
}

Rect::~Rect(void)
{
}


void Rect::draw(void) {

	glBegin(GL_POLYGON);
	  glVertex3f (xpos, ypos, 0.0); // first corner
	  glVertex3f (xpos+width, ypos, 0.0); // second corner
	  glVertex3f (xpos+width, ypos+height, 0.0); // third corner
	  glVertex3f (xpos, ypos+height, 0.0); // fourth corner
	glEnd();
}