// label.h
// class and functions for generating labels

#ifndef TEXT_TO_TEXTURE
#define TEXT_TO_TEXTURE

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_opengl.h>

class Label {
public:
	Label(const char * str,TTF_Font *font, SDL_Color colour);
	GLuint getWidth(void) { return width; }
	GLuint getHeight(void) { return height; }
	GLuint getTex(void) {return texID; }
	static GLuint textToTexture(const char * str,TTF_Font *font, SDL_Color colour,GLuint &width,GLuint &height);
	void draw(GLfloat x, GLfloat y);
	void draw(void);
	float x() { return xpos; }
	float y() { return ypos; }
	void setX(float x) { xpos = x; }
	void setY(float y) { ypos = y; }
private:
	GLuint width;
	GLuint height;
	GLuint texID;
	float xpos;
	float ypos;
};


#endif