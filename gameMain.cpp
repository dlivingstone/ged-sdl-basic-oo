// gameMain.cpp
#include "gameMain.h"

// We should be able to detect when errors occur with SDL if there are
// unrecoverable errors, then we need to print an error message and quit the program
// Note this is NOT a class member
void exitFatalError(char *message)
{
    std::cout << message << " " << SDL_GetError();
    SDL_Quit();
    exit(1);
}

// Static data member of class
int Game::instances = 0;

// This function assumes that strings are dynamic:
// creating and deleting textures for the string
// Strings that remain throughout the game should only be generated once
// or should be generated at compile time and loaded as fixed textures
// Generating textures during init at run time can make it easier to change
// text, while using artist generated textures can allow for a much more
// professional quality finish on the graphics
void Game::displayString(float x, float y, const char * str, TTF_Font * textFont)
{

	SDL_Color colour = { 255, 255, 0 };
	GLuint width, height;
	GLuint tex = Label::textToTexture(str, textFont, colour, width, height);

	// Draw texture here
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glBegin(GL_QUADS);
	  glTexCoord2d(0,1); // Texture has origin at top not bottom
      glVertex3f (x,y, 0.0); // first corner
	  glTexCoord2d(1,1);
	  glVertex3f (x+0.002f*width, y, 0.0); // second corner
	  glTexCoord2d(1,0);
      glVertex3f (x+0.002f*width, y+0.002f*height, 0.0); // third corner
	  glTexCoord2d(0,0);
      glVertex3f (x, y+0.002f*height, 0.0); // fourth corner
    glEnd();

	glDisable(GL_TEXTURE_2D);

	glDeleteTextures(1, &tex);
}

// Constructor method
Game::Game(void)
{
	// We should only have ONE instance of the game class
	// Any more than that, and something has gone wrong somewhere!
	instances++;
	if (instances > 1)
		exitFatalError("Attempt to create multiple game instances");
}

// Destructor method
// Perform any required clean up here
Game::~Game()
{
	TTF_CloseFont(textFont);
    SDL_DestroyWindow(window);
	SDL_Quit();
}

// Set up rendering context
// Sets values for, and creates an OpenGL context for use with SDL
void Game::setupRC(void)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        exitFatalError("Unable to initialize SDL");

    // Request an OpenGL 2.1 context.
	// If you request a context not supported by your drivers, no OpenGL context will be created
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

	// Optional: Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL OpenGL Demo for GED",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate

	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		exitFatalError("TTF failed to initialise.");

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		exitFatalError("Failed to open font.");

//	return window;
}



// Initialise OpenGL values and game related values and variables
void Game::init(void)
{
	setupRC();
	glClearColor(0.0, 0.0, 0.0, 0.0); // set background colour

	std::srand( std::time(NULL) );
	lastTime = clock();

	player = new Rect(0.0f, 0.0f, 0.15f, 0.15f);
	target = new Rect();
	target->setX((float)rand()/RAND_MAX - 0.75f);
	target->setY((float)rand()/RAND_MAX - 0.75f);

	score = 0;

	SDL_Color colour = { 255, 255, 0 };
	playerLabel = new Label("Player",textFont,colour);
	targetLabel = new Label("Target",textFont,colour);
}


// The main rendering function
// In principle, this function should never perform updates to the game
// ONLY render the current state. Reacting to events should be taken care
// of in a seperate update function
void Game::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	// draw player
	glColor3f(1.0,1.0,1.0);
	player->draw();
	// Using the player Label means that we don't have to recreate this texture every frame
	playerLabel->draw(player->x() + (player->w()/2.0), player->y() + player->h());

	//	playerLabel->draw(xpos+(xsize/2.0f), ypos+ysize);

    // draw target
    glColor3f(1.0,0.0,0.0);
	target->draw(); 
	// Using the target Label means that we don't have to recreate this texture every frame
	targetLabel->draw(target->x()+(target->w()/2.0f), target->y()+target->h());

    if ( (target->x() >= player->x()) && (target->x()+target->w() <= player->x()+player->w()) 	// cursor surrounds target in x
		&& (target->y() >= player->y()) && (target->y()+target->h() <= player->y()+player->h() ) ) // cursor surrounds target in y
    {
		score += 100; // congrats, player has scored!
		// randomize the new target position
		target->setX( (float)rand()/RAND_MAX - 0.75f );
		target->setY( (float)rand()/RAND_MAX - 0.75f );
    }

    // Calculate ms/frame
    // Some OpenGL drivers will limit the frames to 60fps (16.66 ms/frame)
    // If so, expect to see the time to rapidly switch between 16 and 17...
    glColor3f(1.0,1.0,1.0);
    currentTime = clock();
    // On some systems, CLOCKS_PER_SECOND is 1000, which makes the arithmetic below redundant
    // - but this is not necessarily the case on all systems
    float milliSecondsPerFrame = ((currentTime - lastTime)/(float)CLOCKS_PER_SEC*1000);

    // Print out the score and frame time information
    std::stringstream strStream;
    strStream << "Score:" << score;
    strStream << " ms/frame: " << milliSecondsPerFrame;
    displayString(-0.9,0.9, strStream.str().c_str(), textFont);
    lastTime = clock();

	SDL_GL_SwapWindow(window); // swap buffers
}

// The event handling function
// In principle, this function should never perform updates to the game
// ONLY detect what events have taken place. Reacting to the events should
// be taken care of in a seperate update function
// This would allow e.g. diagonal movement when two keys are pressed together
// (which is not possible with this implementation)
void Game::handleSDLEvent(SDL_Event const &sdlEvent)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode ;
        //std::cout << ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		switch( sdlEvent.key.keysym.sym )
		{
			case SDLK_UP:
			case 'w': case 'W':
				player->setY(player->y() + 0.05f);
				break;
			case SDLK_DOWN:
			case 's': case 'S':
				player->setY(player->y() - 0.05f);
				break;
			case SDLK_LEFT:
			case 'a': case 'A':
				player->setX(player->x() - 0.05f);
				break;
			case SDLK_RIGHT:
			case 'd': case 'D':
				player->setX(player->x() + 0.05f);
				break;
			default:
				break;
		}
	}
}

// This function contains the main game loop
void Game::run(void) 
{
	bool running = true; // set running to true
	SDL_Event sdlEvent; // variable to detect SDL events

	std::cout << "Progress: About to enter main loop" << std::endl;

	// unlike GLUT, SDL requires you to write your own event loop
	// This puts much more power in the hands of the programmer
	// This simple loop only responds to the window being closed.
	while (running)	// the event loop
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
				handleSDLEvent(sdlEvent);

		}
		//update(); // this is the place to put a call to the game update function
		draw(); // call the draw function
	}

}